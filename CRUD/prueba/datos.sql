
DELETE FROM `categoria`;
INSERT INTO `categoria` (`ID_CATEGORIA`) VALUES
	(1),
	(2),
	(3),
	(4),
	(5),
	(6),
	(7),
	(8),
	(9),
	(10),
	(11),
	(12),
	(13),
	(14),
	(15),
	(16),
	(17),
	(18),
	(19),
	(20),
	(22),
	(23),
	(24),
	(25),
	(29),
	(30),
	(31);

DELETE FROM `color`;
INSERT INTO `color` (`ID_COLOR`) VALUES
	(1),
	(2),
	(3),
	(4),
	(5),
	(6);

DELETE FROM `culture`;
INSERT INTO `culture` (`ID`, `ISO`, `NAME`) VALUES
	(1, 'ES', 'ESPAÑA'),
	(2, 'EN', 'ENGLISH'),
	(3, 'DE', 'DEUTSCH');

DELETE FROM `culture_cat`;
INSERT INTO `culture_cat` (`ID_CATEGORIA`, `ID_CULTURE`, `NOMBRE`) VALUES
	(19, 3, 'dsa'),
	(22, 3, 'dsa'),
	(29, 1, 'COCHES'),
	(29, 2, 'CARS'),
	(29, 3, 'WAGGEN'),
	(30, 1, 'AVIONES'),
	(30, 2, 'PLANES'),
	(30, 3, 'FLUGS'),
	(31, 1, 'BARCAS'),
	(31, 2, 'FSDA'),
	(31, 3, 'FSAD');

DELETE FROM `culture_color`;
INSERT INTO `culture_color` (`ID_COLOR`, `ID_CULTURE`, `NOMBRE`) VALUES
	(1, 1, 'VERDE\r\n'),
	(4, 1, 'AZUL'),
	(4, 2, 'BLUE'),
	(4, 3, 'BLAUEN'),
	(5, 1, 'ROJO'),
	(5, 2, 'RED'),
	(5, 3, 'ROJEN'),
	(6, 1, 'NARANJA'),
	(6, 2, 'FASDFQ'),
	(6, 3, 'FDAS');

DELETE FROM `culture_prod`;
INSERT INTO `culture_prod` (`ID_PRODUCTO`, `ID_CULTURE`, `NOMBRE`, `DESCRIPCION`) VALUES
	(1, 1, 'BMW', 'M3'),
	(1, 2, 'BMW', 'M3'),
	(1, 3, 'BMW', 'M3'),
	(2, 1, 'SUBARU', 'WRX STI'),
	(2, 2, 'SUBARU', 'WRX STI'),
	(2, 3, 'SUBARU', 'WRX STI'),
	(4, 1, 'HONDA', 'NSX'),
	(4, 2, 'HONDA', 'NSX'),
	(4, 3, 'HONDA', 'NSX');

DELETE FROM `producto`;
INSERT INTO `producto` (`ID`, `ID_CATEGORIA`, `PRECIO`) VALUES
	(1, 29, 75000),
	(2, 29, 45000),
	(4, 29, 55000);

DELETE FROM `prod_color`;
INSERT INTO `prod_color` (`ID_PRODUCTO`, `ID_COLOR`) VALUES
	(1, 4),
	(2, 4),
	(4, 1),
	(4, 5),
	(4, 6);

