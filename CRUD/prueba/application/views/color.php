<!DOCTYPE html>
<html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="language" content="es">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Principal</title>

	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />

	<link rel="stylesheet" href="../prueba/Gumby-master/css/gumby.css">
	<link rel="stylesheet" href="../prueba/Gumby-master/css/style.css">
	<script src="../prueba/Gumby-master/js/libs/modernizr-2.6.2.min.js"></script>


	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<header id="header">
		<h1 id="titulo">PRUEBA PROJECT</h1>
		<nav id="nav">
			<ul>
				<li class="titulos"><a href="../">Home</a></li>
				<li class="titulos"><a href="productos">Productos</a></li>
				<li class="titulos"><a href="color">Colores</a></li>
				<li class="titulos"><a href="categoria">Categorias</a></li>
			</ul>
		</nav>
	</header>


	<!-- MODAL PARA EDITAR DATOS A LA BD -->
	<div class="modal" id="modal1">
		<div class="content redondeado">
			<a class="close switch" gumby-trigger="|#modal1"><i class="icon-cancel" /></i></a>
			<div class="row">
				<div class="ten columns centered text-center">
					<h2>Edicion de Colores</h2>
					<div class="formulario">
						<form action="color/actualizar" method="post">
							<table class="sinborde field">
								<tr>
									<td class="formtexto">ID</td><td><input type="text" class="narrow input" id="ide" name="ide" readonly=readonly></td>

									<td>Idioma</td>
									<td><select class="picker" id="idioma" name="idioma">

							<?php if ($idiomas->num_rows() > 0) {  foreach ($idiomas->result() as $row)   {   ?>
											
									<option id="<?php echo $row->ISO; ?>" value="<?php echo $row->ID; ?>"><?php echo $row->ISO; ?></option>

							<?php } } ?>

									</select></td>
								</tr>
								
								<tr><td class="formtexto">COLOR</td>

								<td colspan="3"><input type="text" class="input idiomas" id="color" name="color" required=required></td>
								</tr>
							</table>

							<div class="botones">
								<button type="submit" class="pretty medium success btn ancho100"><i class="icon-check"></i>  Enviar </button>
								<button type="reset" class="pretty medium danger btn ancho100"> <i class="icon-back"></i>  Reset </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- MODAL PARA AÑADIR DATOS A LA BD -->
	<div class="modal" id="modal2">
		<div class="content redondeado">
			<a class="close switch" gumby-trigger="|#modal2"><i class="icon-cancel" /></i></a>
			<div class="row">
				<div class="ten columns centered text-center">
					<h2>Añadir Color a la BD</h2>
					<div class="formulario">
						
						<form action="color/insertar" method="post">
					<table class="sinborde field">

							<?php if ($idiomas->num_rows() > 0) {  foreach ($idiomas->result() as $row)   {   ?>
									<tr><td class="formtexto"><?php echo $row->ISO; ?></td><td><input type="text" class="input" id="color<?php echo $row->ID; ?>" name="color[<?php echo $row->ISO; ?>]" required=required></td></tr>

							<?php } } ?>

							</table>
							<div class="botones">
								<button type="submit" class="pretty medium success btn ancho100"><i class="icon-check"></i>  Enviar </button>
								<button type="reset" class="pretty medium danger btn ancho100"> <i class="icon-back"></i>  Reset </button>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>



	<main id="main">
		<div class="row entrada">
				<div class="twelve columns">
						<h3 class="lead" id="titulo-contenedor">Listado de Colores</h3>
						<div class="medium oval btn info icon-left icon-plus" id="anadir">
							<a href="#" class="switch" gumby-trigger="#modal2">Añadir</a>
						</div>
					<table class="striped rounded" id="listado">
						<thead>
							<tr> 
								<th class="separador" id="id2">ID COLOR</th>
								<th class="separador" id="scolor">COLOR</th>
								<th class="separador" id="opciones">OPCIONES</th>
							</tr>
						</thead>
						<tbody>
							
						<?php
							if ($colores->num_rows() > 0)
							{
							   foreach ($colores->result() as $row)
							   {

							   	?>

							<tr>
								<td class="separador"><?php echo $row->ID_COLOR; ?></td>
								<td class="separador" id="nombre<?php echo $row->ID_COLOR; ?>"><?php echo $row->NOMBRE; ?></td>
								<td class="separador">

									<div class="medium oval btn info">
										<a id="<?php echo $row->ID_COLOR; ?>" href="#" class="switch opciones2" gumby-trigger="#modal1"><i class="icon-pencil"></i></a>
									</div>
								
									<div class="medium oval btn info">
										<a href="color/borrar?id=<?php echo $row->ID_COLOR;?>" ><i class="icon-trash"></i></a>
									</div>
								</td>
							</tr>
							
							<?php
									}
								}

							?>

						</tbody>
					</table>
				</div>
			</div>
	</main>



	<footer id="footer">
		<h4>PRUEBA PROJECT</h4>
	</footer>

	<!-- Grab Google CDN's jQuery, fall back to local if offline -->
	<!-- 2.0 for modern browsers, 1.10 for .oldie -->
	<script>
	var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
	if(!oldieCheck) {
	document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
	} else {
	document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
	}
	</script>
	<script>
	if(!window.jQuery) {
	if(!oldieCheck) {
	  document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
	} else {
	  document.write('<script src="js/libs/jquery-1.10.1.min.js"><\/script>');
	}
	}
	</script>

	<script src="../prueba/js/funciones.js"></script>
	<!--
	Include gumby.js followed by UI modules followed by gumby.init.js
	Or concatenate and minify into a single file -->
	<script gumby-touch="../prueba/Gumby-master/js/libs" src="../prueba/Gumby-master/js/libs/gumby.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.retina.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.fixed.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.skiplink.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.toggleswitch.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.checkbox.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.radiobtn.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.tabs.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/gumby.navbar.js"></script>
	<script src="../prueba/Gumby-master/js/libs/ui/jquery.validation.js"></script>
	<script src="../prueba/Gumby-master/js/libs/gumby.init.js"></script>

	<!--
	Google's recommended deferred loading of JS
	gumby.min.js contains gumby.js, all UI modules and gumby.init.js

	Note: If you opt to use this method of defered loading,
	ensure that any javascript essential to the initial
	display of the page is included separately in a normal
	script tag.

	<script type="text/javascript">
	function downloadJSAtOnload() {
	var element = document.createElement("script");
	element.src = "../prueba/Gumby-master/js/libs/gumby.min.js";
	document.body.appendChild(element);
	}
	if (window.addEventListener)
	window.addEventListener("load", downloadJSAtOnload, false);
	else if (window.attachEvent)
	window.attachEvent("onload", downloadJSAtOnload);
	else window.onload = downloadJSAtOnload;
	</script> -->

	<script src="../prueba/Gumby-master/js/plugins.js"></script>
	<script src="../prueba/Gumby-master/js/main.js"></script>

	<!-- Change UA-XXXXX-X to be your site's ID -->
	<!--<script>
	window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
	Modernizr.load({
	  load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
	});
	</script>-->

	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
	   chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->



  </body>
</html>


	

