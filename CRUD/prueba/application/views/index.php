<!DOCTYPE html>
<html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="language" content="es">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Principal</title>

	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />

	<link rel="stylesheet" href="prueba/Gumby-master/css/gumby.css">
	<link rel="stylesheet" href="prueba/Gumby-master/css/style.css">
	<script src="prueba/Gumby-master/js/libs/modernizr-2.6.2.min.js"></script>


	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<header id="header">
		<h1 id="titulo">PRUEBA PROJECT</h1>
		<nav id="nav">
			<ul>
				<li class="titulos"><a href="../">Home</a></li>
				<li class="titulos"><a href="index.php/productos">Productos</a></li>
				<li class="titulos"><a href="index.php/color">Colores</a></li>
				<li class="titulos"><a href="index.php/categoria">Categorias</a></li>
			</ul>
		</nav>
	</header>

	<main id="main">
		
		<div class="row entrada">
			<h3 class="lead" id="titulo-contenedor">Prueba Project</h3>
				<section class="tabs" id="tabs">
					<ul class="tab-nav">
						<li class="active"><a href="#">Presentacion</a></li>
						<li><a href="#">Contenido</a></li>
						<li><a href="#">Ayuda</a></li>
					</ul>
					<div class="tab-content active">
						<p>Este proyecto contiene un pequeño panel de control (CMS), sus caracteristicas son:</p>
						<table>
							<tr><td> Listar los Productos de la Base de Datos
							<tr><td> Añadir nuevos Productos
							<tr><td> Modificar los ya existentes
							<tr><td> Eliminar productos ya no necesarios o existentes
						</table>
					</div>
					<div class="tab-content">
						<p></p>
						<table>
							<tr><td colspan="2">Tecnologia Usada</td></tr>
							<tr><td>XAMPP v.3.2.1</td><td>Servidor</td></tr>
							<tr><td>CodeIgniter</td><td>Framework PHP</td></tr>
							<tr><td>Gumby</td><td>FrameWork CSS</td></tr>
						</table>
						<table>
							<tr><td>Archivos</td></tr>
							<tr><td><i class="icon-newspaper"></i><a href="prueba/Entidad-Relacion.pdf">Diagrama Entidad Relacion de la Base de Datos</a></td></tr>
						</table>
					</div>
					<div class="tab-content">	
						<p>Llame al 112</p>
					</div>
				</section>	

		</div>
	</main>



	<footer id="footer">
		<h4>PRUEBA PROJECT</h4>
	</footer>



	<!-- Grab Google CDN's jQuery, fall back to local if offline -->
	<!-- 2.0 for modern browsers, 1.10 for .oldie -->
	<script>
	var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
	if(!oldieCheck) {
	document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
	} else {
	document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
	}
	</script>
	<script>
	if(!window.jQuery) {
	if(!oldieCheck) {
	  document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
	} else {
	  document.write('<script src="js/libs/jquery-1.10.1.min.js"><\/script>');
	}
	}
	</script>

	<!--
	Include gumby.js followed by UI modules followed by gumby.init.js
	Or concatenate and minify into a single file -->
	<script gumby-touch="js/libs" src="prueba/Gumby-master/js/libs/gumby.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.retina.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.fixed.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.skiplink.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.toggleswitch.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.checkbox.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.radiobtn.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.tabs.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/gumby.navbar.js"></script>
	<script src="prueba/Gumby-master/js/libs/ui/jquery.validation.js"></script>
	<script src="prueba/Gumby-master/js/libs/gumby.init.js"></script>

	<!--
	Google's recommended deferred loading of JS
	gumby.min.js contains gumby.js, all UI modules and gumby.init.js

	Note: If you opt to use this method of defered loading,
	ensure that any javascript essential to the initial
	display of the page is included separately in a normal
	script tag.

	<script type="text/javascript">
	function downloadJSAtOnload() {
	var element = document.createElement("script");
	element.src = "js/libs/gumby.min.js";
	document.body.appendChild(element);
	}
	if (window.addEventListener)
	window.addEventListener("load", downloadJSAtOnload, false);
	else if (window.attachEvent)
	window.attachEvent("onload", downloadJSAtOnload);
	else window.onload = downloadJSAtOnload;
	</script> -->

	<script src="prueba/Gumby-master/js/plugins.js"></script>
	<script src="prueba/Gumby-master/js/main.js"></script>

	<!-- Change UA-XXXXX-X to be your site's ID -->
	<!--<script>
	window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
	Modernizr.load({
	  load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
	});
	</script>-->

	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
	   chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->



  </body>
</html>
