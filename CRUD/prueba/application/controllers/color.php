<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Color extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// Su propio código de constructor
	}

	public function index()
	{

	        $this->load->model('color_model');

	        $this->load->model('idioma_model');
	        
	        $data['idiomas'] = $this->idioma_model->get();
	        
	        $data['colores'] = $this->color_model->get();      

	        $this->load->view('color', $data);

	}

	public function borrar()
	{
			$id = $_GET['id'];

			$this->load->model('color_model');

   			$this->color_model->delete($id);

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/color','refresh');

	}

	public function insertar()
	{


			$color = $_POST['color'];
			$id_col = null;

			$this->load->model('color_model');
			$id = $this->color_model->post($id_col);

			$this->load->model('colorcult_model');
			foreach ($color as $iso => $dato) {
				$this->colorcult_model->post($id,$iso,$dato);
			}
   			

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/color','refresh');


	}

	public function actualizar()
	{
			
			$id_col = $_POST['ide'];
			$id_cul = $_POST['idioma'];
			$color = $_POST['color'];


			$this->load->model('color_model');
   			$this->color_model->put($id_col,$id_cul,$color);

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/color','refresh');


	}

}

