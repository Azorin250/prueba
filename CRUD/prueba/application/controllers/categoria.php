<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categoria extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// Su propio código de constructor
	}

	public function index()
	{

	        

	        $this->load->model('categoria_model');

	        $this->load->model('idioma_model');

	        $data['categorias'] = $this->categoria_model->get();  

	        $data['idiomas'] = $this->idioma_model->get(); 
	        
	        $this->load->view('categoria', $data);

	}

	public function borrar()
	{
			$id = $_GET['id'];

			$this->load->model('categoria_model');
   			$this->categoria_model->delete($id);

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/categoria','refresh');

	}

	public function insertar()
	{
			$cat = $_POST['cat'];
			$id_cat = null;

			$this->load->model('categoria_model');
			$id = $this->categoria_model->post($id_cat);

			$this->load->model('catcult_model');
			foreach ($cat as $iso => $dato) {
				$this->catcult_model->post($id,$iso,$dato);
			}
   			

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/categoria','refresh');

	}


	public function actualizar()
	{
			$id_cat = $_POST['ide'];
			$id_cul = $_POST['idioma'];
			$cat = $_POST['cat'];


			$this->load->model('categoria_model');

   			$this->categoria_model->put($id_cat,$id_cul,$cat[$id_cul]);

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/categoria','refresh');

	}

}

