<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// Su propio código de constructor
	}

	public function index()
	{

	        $this->load->model('productos_model');

	        $data['query'] = $this->productos_model->get();     

	       	$this->load->model('categoria_model');

	        $data['query2'] = $this->categoria_model->get();  

	        $this->load->model('idioma_model');

	        $data['idiomas'] = $this->idioma_model->get();  

	        $this->load->model('color_model');

	        $data['query3'] = $this->color_model->get();

	        $this->load->model('prodcolor_model');

	        $data['query4'] = $this->prodcolor_model->get();

	        $this->load->view('productos', $data);

	}

	public function borrar()
	{
			$id = $_GET['id'];

			$this->load->model('productos_model');

   			$this->productos_model->delete($id);

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/productos','refresh');

	}

	public function insertar()
	{
			$nombres = $_POST['nomb'];
			$descripcion = $_POST['descri'];
			$precio = $_POST['precio'];
			$cat = $_POST['catE'];
			$color = $_POST['colorE2'];
			$idioma = $_POST['idioma'];

			//INSERTO EL PRODUCTO EN LA TABLA PRODUCTOS (ID, PRECIO Y CATEGORIA)
			$this->load->model('productos_model');
   			$this->productos_model->post($precio,$cat);
   			$id = $this->db->insert_id();


   			//INSERTO EL PRODUCTO CON LOS DATOS EN SU RESPECTIVO IDIOMA (ID_PRODUCTO, ID_CULTURE, NOMBRE, DESCRIPCION)
   			$this->load->model('prodcult_model');
   			foreach ($nombres as $key => $nombre) {
   				$this->prodcult_model->post($id,$idioma[$key],$nombre,$descripcion[$key]);
   			}


   			//INSERTO EL COLOR DEL PRODUCTO
   			$this->load->model('prodcolor_model');
   			foreach ($color as $key => $value) {
   				$this->prodcolor_model->post($id,$value);
   			}


   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/productos','refresh');

	}

	public function actualizar()
	{
			$id = $_POST['ide'];
			$nombre = $_POST['nomb'];
			$descripcion = $_POST['descri'];
			$precio = $_POST['precio'];
			$cat = $_POST['catE'];
			$color = $_POST['colorE2'];
			$iso = $_POST['idioma'];



			//ACTUALIZO EL PRODUCTO EN LA TABLA PRODUCTOS (ID, PRECIO Y CATEGORIA)
			$this->load->model('productos_model');
   			$this->productos_model->put($id,$precio,$cat);

   			//BORRO TODOS LOS COLORES DEL PRODUCTO
   			$this->load->model('prodcolor_model');
   			$this->prodcolor_model->delete($id);

   			//INSERTO LOS COLORES NUEVOS DEL PRODUCTO
   			foreach ($color as $key => $value) {
   				$this->prodcolor_model->post($id,$value);
   			}

   			//INSERTO EL PRODUCTO CON LOS DATOS EN SU RESPECTIVO IDIOMA (ID_PRODUCTO, ID_CULTURE, NOMBRE, DESCRIPCION)
   			$this->load->model('prodcult_model');
   			$this->prodcult_model->put($id,$iso,$nombre,$descripcion);

   			

   			$this->load->helper('url');
			redirect('http://prueba-fran.esy.es/index.php/productos','refresh');

	}

}

