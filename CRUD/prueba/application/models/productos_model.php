<?php

class Productos_model extends CI_Model{

	var $lista;

	function __construct()
	{
		// Llamando al contructor del Modelo
		parent::__construct();
	}

	
	function get($iso = 'ES')	//OBTENER
	{


		$sql = "SELECT producto.ID, culture_prod.NOMBRE, culture_prod.DESCRIPCION, culture_cat.NOMBRE as CATEGORIA,  producto.PRECIO
				
				FROM producto
				LEFT JOIN culture_prod
				ON producto.ID = culture_prod.ID_PRODUCTO
				LEFT JOIN culture
				ON culture_prod.ID_CULTURE = culture.ID
				
				LEFT JOIN categoria
				ON producto.ID_CATEGORIA = categoria.ID_CATEGORIA
				LEFT JOIN culture_cat
				ON categoria.ID_CATEGORIA = culture_cat.ID_CATEGORIA 
				
				WHERE culture.ID = culture_cat.ID_CULTURE and culture.ISO ='".$iso."'
				 
				ORDER BY producto.ID";

		$lista = $this->db->query($sql);

		return $lista;	
	}
	
	function delete($id)	//BORRAR
	{
		$this->db->where('ID_PRODUCTO', $id);
		$this->db->delete('prod_color');

		$this->db->where('ID', $id);
		$this->db->delete('producto');

	}

	function post($precio,$cat)	//INSERTAR
	{
		$datos = new stdClass();
		
		$datos->PRECIO = $precio;
		$datos->ID_CATEGORIA = $cat;

		$this->db->insert('producto', $datos);
	}

	function put($id,$precio,$cat)	//ACTUALIZAR
	{
		$datos = new stdClass();

		$datos->PRECIO = $precio;
		$datos->ID_CATEGORIA = $cat;

		$this->db->where('ID', $id);
		$this->db->update('producto', $datos);

	}




}

?>