<?php

class Color_model extends CI_Model{

	var $lista;

	function __construct()
	{
		// Llamando al contructor del Modelo
		parent::__construct();
	}

	
	function get($iso = 'ES')	//OBTENER
	{
		$sql = "SELECT ID_COLOR, ID_CULTURE, NOMBRE FROM culture_color, culture 
				WHERE culture.ID = culture_color.ID_CULTURE AND culture.ISO ='".$iso."'";
		$lista = $this->db->query($sql);

		return $lista;
	}

	function delete($id)	//BORRAR
	{
		$this->db->where('ID_COLOR', $id);
		$this->db->delete('prod_color');

		$this->db->where('ID_COLOR', $id);
		$this->db->delete('color');

		$this->db->where('ID_COLOR', $id);
		$this->db->delete('culture_color');

	}

	function post($id)	//INSERTAR
	{
		
		$datos = new stdClass();
		$datos->ID_COLOR = $id;

		$this->db->insert('color',$datos);

		$last = $this->db->query("SELECT MAX(ID_COLOR) AS LAST FROM color;");
		
		foreach ($last->result() as $row) {
			$id = $row->LAST; 
		}

		return $id;
	}

	function put($id_col,$id_cul,$col)	//ACTUALIZAR
	{
		$datos = new stdClass();
		$datos->ID_COLOR = $id_col;
		$datos->ID_CULTURE = $id_cul;
		$datos->NOMBRE = $col;

		$this->db->where('ID_CULTURE', $id_cul);
		$this->db->where('ID_COLOR', $id_col);
		$this->db->update('culture_color', $datos);

	}




}

?>