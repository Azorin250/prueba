<?php

class Prodcolor_model extends CI_Model{

	var $lista;

	function __construct()
	{
		// Llamando al contructor del Modelo
		parent::__construct();
	}

	
	function get($iso = 1)	//OBTENER
	{
		/*$sql = "SELECT * FROM prodcolor"; 
		$lista = $this->db->query($sql);*/

		$sql = "SELECT prod_color.ID_PRODUCTO, prod_color.ID_COLOR, culture_color.NOMBRE FROM prod_color 				
				LEFT JOIN color
				ON prod_color.ID_COLOR = color.ID_COLOR
				LEFT JOIN culture_color
				ON color.ID_COLOR = culture_color.ID_COLOR
				WHERE culture_color.ID_CULTURE = ".$iso;
				//WHERE ID_PRODUCTO = "+$id; 

		$lista = $this->db->query($sql);

		return $lista;
	}
	
	function delete($id = NULL,$color = NULL)	//BORRAR
	{
		
		if(isset($id)){
			$this->db->where('ID_PRODUCTO', $id);
			$this->db->delete('prod_color');
		}

		if(isset($color)){
			$this->db->where('ID_COLOR', $color);
			$this->db->delete('prod_color');
		}
	}

	function post($id,$color)	//INSERTAR
	{
		$datos = new stdClass();
		$datos->ID_PRODUCTO = $id;
		$datos->ID_COLOR = $color;

		$this->db->insert('prod_color', $datos);
	}

	function put($id,$color)	//UPDATE
	{
		$datos = new stdClass();
		$datos->ID_COLOR = $color;

		$this->db->where('ID_PRODUCTO', $id);
		$this->db->update('prod_color', $datos);

	}



}

?>