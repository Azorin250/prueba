
<?php

class Categoria_model extends CI_Model{

	var $lista;

	function __construct()
	{

		// Llamando al contructor del Modelo
		parent::__construct();
	}

	
	function get($iso = 'ES')	//OBTENER
	{
		$sql = "SELECT ID_CATEGORIA, ID_CULTURE, NOMBRE FROM culture_cat, culture 
				WHERE culture.ID = culture_cat.ID_CULTURE AND culture.ISO ='".$iso."'";

		$lista = $this->db->query($sql);

		return $lista;
	}

	function delete($id)	//BORRAR
	{
		
		$this->db->where('ID_CATEGORIA', $id);
		$this->db->delete('producto');
		
		$this->db->where('ID_CATEGORIA', $id);
		$this->db->delete('categoria');

		$this->db->where('ID_CATEGORIA', $id);
		$this->db->delete('culture_cat');

	}
	function post($id)	//INSERTAR
	{
		$datos = new stdClass();
		
		$datos->ID_CATEGORIA = $id;

		$this->db->insert('categoria',$datos);

		$last = $this->db->query("SELECT MAX(ID_CATEGORIA) AS LAST FROM categoria;");
		
		foreach ($last->result() as $row) {
			$id = $row->LAST; 
		}

		return $id;

	}

	function put($id_cat,$id_cul,$cat)	//ACTUALIZAR
	{
		$datos = new stdClass();
		$datos->ID_CATEGORIA = $id_cat;
		$datos->ID_CULTURE = $id_cul;
		$datos->NOMBRE = $cat;

		$this->db->where('ID_CULTURE', $id_cul);
		$this->db->where('ID_CATEGORIA', $id_cat);
		$this->db->update('culture_cat', $datos);

	}



}

?>